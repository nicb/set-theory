#
# $Id: contour-retro.awk 38 2014-02-12 14:44:53Z nicb $
#
# Morris' contour retrograde function
#
#	Command line:
#		awk -f contour-retro.awk \
#			[top_note=n] [convert_fields="x,y,..."] <file>
#
#	pitches can be in different colon-separated fields which can
#	be defined in a comma-separated string contained in the variable
#	convert_fields (convert_fields defaults to "1")
#	pitches are space-separated numbers
#	contour retrogression is performed on a given c-space order (bandwidth)
#	which can be specified with the top_note variable; if no c-space
#	order is specified, or if the c-space order specified is smaller
#	than the c-space order required in order to perform contour retrogression
#	then the minimum c-space order is used
#
function retrograde_contour(line,
	intvs, lints, maxhtnote, minhtnote, _top_, i)
{
	intvs[0] = split (line, intvs, " ");
	i = intvs[0];

	while (i)
	{
		printf "%-d", intvs[i--];
		if (i)
			printf " ";
	}

	delete_array(intvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%s", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			retrograde_contour($i);
		else
			printf "%s", $i;
		if (i+1 <= NF)
			printf ":";
	}
	printf "\n";
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;
	top_note = -10000;
}
#
# $Log: contour-retro.awk,v $
# Revision 0.0  1996/04/20 20:43:40  nicb
# Initial Revision
#
#
