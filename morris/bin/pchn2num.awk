#
# $Id: pchn2num.awk 38 2014-02-12 14:44:53Z nicb $
#
#	convert pitch names into absolute numbers with offset 0 at c3
#	pitches can be in different colon-separated fields which can
#	be defined in a comma-separated string contained in the variable
#	convert_fields
#	pitches are space-separated strings
#
function convert(note)
{
	return key[note];
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%-d", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			convert_f($i);
		else
			printf("%s", $i);
		if (i+1 <= NF)
			printf ":";
	}

	printf("\n");
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;

# octave -1

	key["cf-1"] =-49;
	key["c-1"] =-48;
	key["c#-1"] =-47;
	key["df-1"] =-47;
	key["d-1"] =-46;
	key["d#-1"] =-45;
	key["ef-1"] =-45;
	key["e-1"] =-44;
	key["e#-1"] =-43;
	key["ff-1"] =-44;
	key["f-1"] =-43;
	key["f#-1"] =-42;
	key["gf-1"] =-42;
	key["g-1"] =-41;
	key["g#-1"] =-40;
	key["af-1"] =-40;
	key["a-1"] =-39;
	key["a#-1"] =-38;
	key["bf-1"] =-38;
	key["b-1"] =-37;
	key["b#-1"] =-36;

# octave 0

	key["cf0"] =-37;
	key["c0"] =-36;
	key["c#0"] =-35;
	key["df0"] =-35;
	key["d0"] =-34;
	key["d#0"] =-33;
	key["ef0"] =-33;
	key["e0"] =-32;
	key["e#0"] =-31;
	key["ff0"] =-32;
	key["f0"] =-31;
	key["f#0"] =-30;
	key["gf0"] =-30;
	key["g0"] =-29;
	key["g#0"] =-28;
	key["af0"] =-28;
	key["a0"] =-27;
	key["a#0"] =-26;
	key["bf0"] =-26;
	key["b0"] =-25;
	key["b#0"] =-24;

# octave 1

	key["cf1"] =-25;
	key["c1"] =-24;
	key["c#1"] =-23;
	key["df1"] =-23;
	key["d1"] =-22;
	key["d#1"] =-21;
	key["ef1"] =-21;
	key["e1"] =-20;
	key["e#1"] =-19;
	key["ff1"] =-20;
	key["f1"] =-19;
	key["f#1"] =-18;
	key["gf1"] =-18;
	key["g1"] =-17;
	key["g#1"] =-16;
	key["af1"] =-16;
	key["a1"] =-15;
	key["a#1"] =-14;
	key["bf1"] =-14;
	key["b1"] =-13;
	key["b#1"] =-12;

# octave 2

	key["cf2"] =-13;
	key["c2"] =-12;
	key["c#2"] =-11;
	key["df2"] =-11;
	key["d2"] =-10;
	key["d#2"] =-9;
	key["ef2"] =-9;
	key["e2"] =-8;
	key["e#2"] =-7;
	key["ff2"] =-8;
	key["f2"] =-7;
	key["f#2"] =-6;
	key["gf2"] =-6;
	key["g2"] =-5;
	key["g#2"] =-4;
	key["af2"] =-4;
	key["a2"] =-3;
	key["a#2"] =-2;
	key["bf2"] =-2;
	key["b2"] =-1;
	key["b#2"] =0;

# central octave

	key["cf3"] = -1;
	key["c3"] = 0;
	key["c#3"] = 1;
	key["df3"] = 1;
	key["d3"] = 2;
	key["d#3"] = 3;
	key["ef3"] = 3;
	key["e3"] = 4;
	key["e#3"] = 5;
	key["ff3"] = 4;
	key["f3"] = 5;
	key["f#3"] = 6;
	key["gf3"] = 6;
	key["g3"] = 7;
	key["g#3"] = 8;
	key["af3"] = 8;
	key["a3"] = 9;
	key["a#3"] = 10;
	key["bf3"] = 10;
	key["b3"] = 11;
	key["b#3"] = 12;

# octave 4

	key["cf4"] = 11;
	key["c4"] =  12;
	key["c#4"] =  13;
	key["df4"] =  13;
	key["d4"] =  14;
	key["d#4"] =  15;
	key["ef4"] =  15;
	key["e4"] =  16;
	key["e#4"] =  17;
	key["ff4"] =  16;
	key["f4"] =  17;
	key["f#4"] =  18;
	key["gf4"] =  18;
	key["g4"] =  19;
	key["g#4"] =  20;
	key["af4"] =  20;
	key["a4"] =  21;
	key["a#4"] =  22;
	key["bf4"] =  22;
	key["b4"] =  23;
	key["b#4"] =  24;

# octave 5

	key["cf5"] =  23;
	key["c5"] = 24;
	key["c#5"] = 25;
	key["df5"] = 25;
	key["d5"] = 26;
	key["d#5"] = 27;
	key["ef5"] = 27;
	key["e5"] = 28;
	key["e#5"] = 29;
	key["ff5"] = 28;
	key["f5"] = 29;
	key["f#5"] = 30;
	key["gf5"] = 30;
	key["g5"] = 31;
	key["g#5"] = 32;
	key["af5"] = 32;
	key["a5"] = 33;
	key["a#5"] = 34;
	key["bf5"] = 34;
	key["b5"] = 35;
	key["b#5"] = 36;

# octave 6

	key["cf6"] = 35;
	key["c6"] = 36;
	key["c#6"] = 37;
	key["df6"] = 37;
	key["d6"] = 38;
	key["d#6"] = 39;
	key["ef6"] = 39;
	key["e6"] = 40;
	key["e#6"] = 41;
	key["ff6"] = 40;
	key["f6"] = 41;
	key["f#6"] = 42;
	key["gf6"] = 42;
	key["g6"] = 43;
	key["g#6"] = 44;
	key["af6"] = 44;
	key["a6"] = 45;
	key["a#6"] = 46;
	key["bf6"] = 46;
	key["b6"] = 47;
	key["b#6"] = 48;

# octave 7

	key["cf7"] = 47;
	key["c7"] = 48;
	key["c#7"] = 49;
	key["df7"] = 49;
	key["d7"] = 50;
	key["d#7"] = 51;
	key["ef7"] = 51;
	key["e7"] = 52;
	key["e#7"] = 53;
	key["ff7"] = 52;
	key["f7"] = 53;
	key["f#7"] = 54;
	key["gf7"] = 54;
	key["g7"] = 55;
	key["g#7"] = 56;
	key["af7"] = 56;
	key["a7"] = 57;
	key["a#7"] = 58;
	key["bf7"] = 58;
	key["b7"] = 59;
	key["b#7"] = 60;
}
#
# $Log: pchn2num.awk,v $
# Revision 0.1  1996/04/20 11:10:41  nicb
# added multiple-field conversion
#
# Revision 0.0  1996/04/17 22:12:28  nicb
# Initial Revision
#
#
