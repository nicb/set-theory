#
# $Id: contour-inversion.awk 38 2014-02-12 14:44:53Z nicb $
#
# Morris' contour inversion function
#
#	Command line:
#		awk -f contour-inversion.awk \
#			[top_note=n] [convert_fields="x,y,..."] <file>
#
#	pitches can be in different colon-separated fields which can
#	be defined in a comma-separated string contained in the variable
#	convert_fields (convert_fields defaults to "1")
#	pitches are space-separated numbers
#	contour inversion is performed on a given c-space order (bandwidth)
#	which can be specified with the top_note variable; if no c-space
#	order is specified, or if the c-space order specified is smaller
#	than the c-space order required in order to perform contour inversion
#	then the minimum c-space order is used
#
function abs(n)
{
	return (n < 0) ? -n : n;
}
function get_maxhtnote(line,
	i, result)
{
	result = -10000;

	for (i = 1; i <= line[0]; ++i)
		if (line[i] > result)
			result = line[i];

	return result;
}
function get_minhtnote(line,
	i, result)
{
	result = 10000;

	for (i = 1; i <= line[0]; ++i)
		if (line[i] < result)
			result = line[i];

	return result;
}
function invert_contour(line,
	intvs, lints, maxhtnote, minhtnote, _top_, i)
{
	intvs[0] = split (line, intvs, " ");
	maxhtnote = get_maxhtnote(intvs);
	minhtnote = get_minhtnote(intvs);
	width = maxhtnote - minhtnote;
	_top_ = ((maxhtnote > top_note) ? maxhtnote : top_note);

	for (i = 1; i <= intvs[0]; ++i)
	{
		printf "%-d", _top_ + (minhtnote - intvs[i]);
		if (i+1 <= intvs[0])
			printf " ";
	}

	delete_array(intvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%s", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			invert_contour($i);
		else
			printf "%s", $i;
		if (i+1 <= NF)
			printf ":";
	}
	printf "\n";
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;
	top_note = -10000;
}
#
# $Log: contour-inversion.awk,v $
# Revision 0.0  1996/04/20 18:10:38  nicb
# Initial Revision (different algorithm than on pg. 29)
#
#
