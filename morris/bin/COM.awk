#
# $Id: COM.awk 38 2014-02-12 14:44:53Z nicb $
#
# Morris' COM matrix
#
#	Command line:
#		awk -f COM.awk [convert_fields="1,2,..."] <file>
#
#	pitches can be in different colon-separated fields which can
#	be defined in a comma-separated string contained in the variable
#	convert_fields (convert_fields defaults to "1")
#	pitches are space-separated numbers
#
function print_COM(level, line,
	i)
{
	printf "%4d |", level;

	for (i = 1; i <= line[0]; ++i)
	{
		if (level == line[i])
			printf "  0 ";
		else if (level > line[i])
			printf "  - ";
		else
			printf "  + ";
	}

	printf "\n";
}
function print_COM_head(notes,
	i)
{
	printf " COM |";
	for (i = 1; i <= notes[0]; ++i)
		printf "%3d ", notes[i];
	printf "\n-----+";
	for (i = 0; i <= notes[0]; ++i)
		printf "----";
	printf "\n";
}
function COM(line,
	intvs, lints, maxhtnote, minhtnote, i)
{
	intvs[0] = split (line, intvs, " ");

	print_COM_head(intvs);

	for (i = 1; i <= intvs[0]; ++i)
		print_COM(intvs[i], intvs);

	delete_array(intvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%s", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			COM($i);
		if (i+1 <= NF)
			printf "\n";
	}
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;
}
#
# $Log: COM.awk,v $
# Revision 0.1  1996/04/20 18:10:03  nicb
# corrected a bug, better formatting
#
# Revision 0.0  1996/04/20 16:00:27  nicb
# Initial Revision
#
#
