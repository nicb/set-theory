#
# $Id: t-matrix.awk 38 2014-02-12 14:44:53Z nicb $
#
# t-matrix.awk: build the t-matrix formed by ip<Ax,By> (Morris, pg.49)
#
#	awk -f t-matrix.awk <file>
#
# where <file> is made out of lines of colon-separated records (2 per line)
# which are:
#	<A0 A1...An>:<B0 B1...Bn>
#
# if no second field is given, the matrix is self-referenced on A pitches
#
function print_header(line,
	i)
{
	printf "  T  |";
	for (i = 1; i <= line[0]; ++i)
	{
		printf("%4d", line[i]);
		if (i+1 <= line[0])
			printf " ";
	}
	printf "\n-----+";
	for (i = 1; i <= line[0]; ++i)
		printf "-----";
	printf "\n";
}
function transpose_matrix(A, B,
	i, j)
{
	print_header(B);

	for (i = 1; i <= A[0]; ++i)
	{
		printf "%4d |", A[i];
		for (j = 1; j <= B[0]; ++j)
		{
			printf "%4d", -(A[i])+B[j];
			if (j+1 <= B[0])
				printf " ";
		}
		printf "\n";
	}
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
{
	A_pitches[0] = split($1, A_pitches, " ");
	if (NF > 1)
		B_pitches[0] = split($2, B_pitches, " ");
	else
		B_pitches[0] = split($1, B_pitches, " ");

	transpose_matrix(A_pitches, B_pitches);

	delete_array(A_pitches);
	delete_array(B_pitches);
}
BEGIN {
	FS=":";
	A_pitches[0] = 0;
	B_pitches[0] = 0;
}
#
# $Log: t-matrix.awk,v $
# Revision 0.0  1996/04/20 20:43:40  nicb
# Initial Revision
#
#
