#
# $Id: contour.awk 38 2014-02-12 14:44:53Z nicb $
#
# Morris' contour matrix
#
#	Command line:
#		awk -f contour.awk [convert_fields="1,2,..."] <file>
#
#	pitches can be in different colon-separated fields which can
#	be defined in a comma-separated string contained in the variable
#	convert_fields (convert_fields defaults to "1")
#	pitches are space-separated numbers
#
function print_contour(level, line,
	i)
{
	printf "%3d |", level;

	for (i = 1; i <= line[0]; ++i)
	{
		if (level == line[i])
			printf " X ";
		else
			printf "   ";
	}

	printf "\n";
}
function print_contour_base(num_notes,
	i)
{
	printf "    ";
	for (i = 0; i < num_notes; ++i)
		printf "---";
	printf "\n     ";
	for (i = 0; i < num_notes; ++i)
		printf "%2d ", i;
	printf "\n";
}
function get_maxhtnote(line,
	i, result)
{
	result = 0;

	for (i = 1; i <= line[0]; ++i)
		if (line[i] > result)
			result = line[i];

	return result;
}
function get_minhtnote(line,
	i, result)
{
	result = 10000;

	for (i = 1; i <= line[0]; ++i)
		if (line[i] < result)
			result = line[i];

	return result;
}
function contour(line,
	intvs, lints, maxhtnote, minhtnote, i)
{
	intvs[0] = split (line, intvs, " ");
	maxhtnote = get_maxhtnote(intvs);
	minhtnote = get_minhtnote(intvs);

	while (maxhtnote >= minhtnote)
		print_contour(maxhtnote--, intvs);

	print_contour_base(intvs[0]);

	delete_array(intvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%s", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			contour($i);
		if (i+1 <= NF)
			printf "\n";
	}
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;
}
#
# $Log: contour.awk,v $
# Revision 0.0  1996/04/20 16:00:27  nicb
# Initial Revision
#
#
