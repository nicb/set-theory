#
# $Id: t.awk 38 2014-02-12 14:44:53Z nicb $
#
# t.awk: transpose the incoming pattern (in numeric form) to the given
# transposition factor - command line:
#
#	awk -f t.awk t=<n> <file>
#
function print_transpose(line,
	i)
{
	for (i = 1; i <= line[0]; ++i)
	{
		printf("%d", line[i]);
		if (i+1 <= line[0])
			printf " ";
	}

	printf "\n";
}
function transpose(line, tfact,
	intvs, i)
{
	intvs[0] = split (line, intvs, " ");

	for (i = 1; i <= intvs[0]; ++i)
		intvs[i] += tfact;

	print_transpose(intvs);
	delete_array(intvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
{
	transpose($0,t);
}
BEGIN {
	FS=" ";
	t=0;
}
#
# $Log: t.awk,v $
# Revision 0.2  1996/05/05 12:58:48  nicb
# version that works with direct transposition
#
# Revision 0.1  1996/04/20 14:39:25  nicb
# added: multiple fields handling
# 	  multiple transposition matrix
#
# Revision 0.0  1996/04/17 22:12:28  nicb
# Initial Revision
#
