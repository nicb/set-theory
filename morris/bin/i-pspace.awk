#
# $Id: i-pspace.awk 38 2014-02-12 14:44:53Z nicb $
#
# Morris' inversion function in pspace (that is, dividing the range in
#	half and using that as inversional center)
#
#	Command line:
#		awk -f i-pspace.awk [convert_fields="x,y,..."] <file>
#
#	pitches can be in different colon-separated fields which can
#	be defined in a comma-separated string contained in the variable
#	convert_fields (convert_fields defaults to "1")
#	pitches are space-separated numbers
#
function get_maxhtnote(line,
	i, result)
{
	result = -10000;

	for (i = 1; i <= line[0]; ++i)
		if (line[i] > result)
			result = line[i];

	return result;
}
function get_minhtnote(line,
	i, result)
{
	result = 10000;

	for (i = 1; i <= line[0]; ++i)
		if (line[i] < result)
			result = line[i];

	return result;
}
function invert(line,
	intvs, lints, maxhtnote, minhtnote, i)
{
	intvs[0] = split (line, intvs, " ");
	maxhtnote = get_maxhtnote(intvs);
	minhtnote = get_minhtnote(intvs);
	width = maxhtnote - minhtnote;

	for (i = 1; i <= intvs[0]; ++i)
	{
		printf "%-d", maxhtnote + (minhtnote - intvs[i]);
		if (i+1 <= intvs[0])
			printf " ";
	}

	delete_array(intvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%s", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			invert($i);
		else
			printf "%s", $i;
		if (i+1 <= NF)
			printf ":";
	}
	printf "\n";
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;
}
#
# $Log: i-pspace.awk,v $
# Revision 0.0  1996/04/20 20:43:40  nicb
# Initial Revision
#
#
