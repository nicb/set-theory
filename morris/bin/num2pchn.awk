#
# $Id: num2pchn.awk 38 2014-02-12 14:44:53Z nicb $
#
function convert(num,
	mtranspose)
{
	mtranspose = num+offset;
	mtranspose = (mtranspose < 1) ? 1 : ((mtranspose > max_note) ? max_note : mtranspose);
	return key[mtranspose];
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%s", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			convert_f($i);
		else
			printf("%s", $i);
		if (i+1 <= NF)
			printf ":";
	}

	printf("\n");
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;

	offset=49;

# octave -1

#	key[0] = "cf-1";
	key[1] = "c-1";
	key[2] = "c#-1";
#	key[2] = "df-1";
	key[3] = "d-1";
	key[4] = "d#-1";
#	key[4] = "ef-1";
	key[5] = "e-1";
#	key[6] = "e#-1";
#	key[5] = "ff-1";
	key[6] = "f-1";
	key[7] = "f#-1";
#	key[7] = "gf-1";
	key[8] = "g-1";
	key[9] = "g#-1";
#	key[9] = "af-1";
	key[10] = "a-1";
	key[11] = "a#-1";
#	key[11] = "bf-1";
	key[12] = "b-1";
#	key[13] = "b#-1";

# octave 0

#	key[12] = "cf0";
	key[13] = "c0";
	key[14] = "c#0";
#	key[14] = "df0";
	key[15] = "d0";
	key[16] = "d#0";
#	key[16] = "ef0";
	key[17] = "e0";
#	key[18] = "e#0";
#	key[17] = "ff0";
	key[18] = "f0";
	key[19] = "f#0";
#	key[19] = "gf0";
	key[20] = "g0";
	key[21] = "g#0";
#	key[21] = "af0";
	key[22] = "a0";
	key[23] = "a#0";
#	key[23] = "bf0";
	key[24] = "b0";
#	key[25] = "b#0";

# octave 1

#	key[24] = "cf1";
	key[25] = "c1";
	key[26] = "c#1";
#	key[26] = "df1";
	key[27] = "d1";
	key[28] = "d#1";
#	key[28] = "ef1";
	key[29] = "e1";
#	key[30] = "e#1";
#	key[29] = "ff1";
	key[30] = "f1";
	key[31] = "f#1";
#	key[31] = "gf1";
	key[32] = "g1";
	key[33] = "g#1";
#	key[33] = "af1";
	key[34] = "a1";
	key[35] = "a#1";
#	key[35] = "bf1";
	key[36] = "b1";
#	key[37] = "b#1";

# octave 2

#	key[36] = "cf2";
	key[37] = "c2";
	key[38] = "c#2";
#	key[38] = "df2";
	key[39] = "d2";
	key[40] = "d#2";
#	key[40] = "ef2";
	key[41] = "e2";
#	key[42] = "e#2";
#	key[41] = "ff2";
	key[42] = "f2";
	key[43] = "f#2";
#	key[43] = "gf2";
	key[44] = "g2";
	key[45] = "g#2";
#	key[45] = "af2";
	key[46] = "a2";
	key[47] = "a#2";
#	key[47] = "bf2";
	key[48] = "b2";
#	key[49] = "b#2";

# central octave

#	key[48] = "cf3";
	key[49] = "c3";
	key[50] = "c#3";
#	key[50] = "df3";
	key[51] = "d3";
	key[52] = "d#3";
#	key[52] = "ef3";
	key[53] = "e3";
#	key[54] = "e#3";
#	key[53] = "ff3";
	key[54] = "f3";
	key[55] = "f#3";
#	key[55] = "gf3";
	key[56] = "g3";
	key[57] = "g#3";
#	key[57] = "af3";
	key[58] = "a3";
	key[59] = "a#3";
#	key[59] = "bf3";
	key[60] = "b3";
#	key[61] = "b#3";

# octave 4

#	key[60] = "cf4";
	key[61] = "c4";
	key[62] = "c#4";
#	key[62] = "df4";
	key[63] = "d4";
	key[64] = "d#4";
#	key[64] = "ef4";
	key[65] = "e4";
#	key[66] = "e#4";
#	key[65] = "ff4";
	key[66] = "f4";
	key[67] = "f#4";
#	key[67] = "gf4";
	key[68] = "g4";
	key[69] = "g#4";
#	key[69] = "af4";
	key[70] = "a4";
	key[71] = "a#4";
#	key[71] = "bf4";
	key[72] = "b4";
#	key[73] = "b#4";

# octave 5

#	key[72] = "cf5";
	key[73] = "c5";
	key[74] = "c#5";
#	key[74] = "df5";
	key[75] = "d5";
	key[76] = "d#5";
#	key[76] = "ef5";
	key[77] = "e5";
#	key[78] = "e#5";
#	key[77] = "ff5";
	key[78] = "f5";
	key[79] = "f#5";
#	key[79] = "gf5";
	key[80] = "g5";
	key[81] = "g#5";
#	key[81] = "af5";
	key[82] = "a5";
	key[83] = "a#5";
#	key[83] = "bf5";
	key[84] = "b5";
#	key[85] = "b#5";

# octave 6

#	key[84] = "cf6";
	key[85] = "c6";
	key[86] = "c#6";
#	key[86] = "df6";
	key[87] = "d6";
	key[88] = "d#6";
#	key[88] = "ef6";
	key[89] = "e6";
#	key[90] = "e#6";
#	key[89] = "ff6";
	key[90] = "f6";
	key[91] = "f#6";
#	key[91] = "gf6";
	key[92] = "g6";
	key[93] = "g#6";
#	key[93] = "af6";
	key[94] = "a6";
	key[95] = "a#6";
#	key[95] = "bf6";
	key[96] = "b6";
#	key[97] = "b#6";

# octave 7

#	key[96] = "cf7";
	key[97] = "c7";
	key[98] = "c#7";
#	key[98] = "df7";
	key[99] = "d7";
	key[100] = "d#7";
#	key[100] = "ef7";
	key[101] = "e7";
#	key[102] = "e#7";
#	key[101] = "ff7";
	key[102] = "f7";
	key[103] = "f#7";
#	key[103] = "gf7";
	key[104] = "g7";
	key[105] = "g#7";
#	key[105] = "af7";
	key[106] = "a7";
	key[107] = "a#7";
#	key[107] = "bf7";
	key[108] = "b7";
#	key[109] = "b#7";

	max_note = 108;
}
#
# $Log: num2pchn.awk,v $
# Revision 0.2  1996/04/20 14:20:35  nicb
# band-limiting security checks
#
# Revision 0.1  1996/04/20 11:10:41  nicb
# added multiple-field conversion
#
# Revision 0.0  1996/04/17 22:12:28  nicb
# Initial Revision
#
#
