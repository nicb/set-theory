#
# $Id: r.awk 38 2014-02-12 14:44:53Z nicb $
#
# r.awk: retrograde the incoming pattern (in numeric form)
#
#	awk -f r.awk <file>
#
function print_retrograde(line,
	i)
{
	for (i = 1; i <= line[0]; ++i)
	{
		printf("%d", line[i]);
		if (i+1 <= line[0])
			printf " ";
	}

	printf "\n";
}
function retrograde(line,
	intvs, rtvs, i, n)
{
	intvs[0] = rtvs[0] = n = split (line, intvs, " ");
	++n;

	for (i = 1; i <= intvs[0]; ++i)
		rtvs[i] = intvs[n-i];

	print_retrograde(rtvs);
	delete_array(intvs);
	delete_array(rtvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
{
	retrograde($0);
}
BEGIN {
	FS=" ";
	t=0;
}
#
# $Log: t.awk,v $
#
