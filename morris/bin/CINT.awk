#
# $Id: CINT.awk 38 2014-02-12 14:44:53Z nicb $
#
# Morris' CINT function
#
#	Command line:
#		awk -f CINT.awk <file>
#
#	pitches can be in different colon-separated fields which can
#	be defined in a comma-separated string contained in the variable
#	convert_fields
#	pitches are space-separated numbers
#
function print_pcyc(line,
	i)
{
	if (contours)
		printf("<");

	for (i = 1; i <= line[0]; ++i)
	{
		printf("%d", line[i]);
		if (i+1 <= line[0])
			printf " ";
	}

	if (contours)
		printf " ->";
}
function pcyc(line,
	intvs, lints, i)
{
	intvs[0] = split (line, intvs, " ");

	for (i = 2; i <= intvs[0]; ++i)
		lints[i-1] = intvs[i]-intvs[i-1];

	lints[i-1] = intvs[1] - intvs[i-1];

	lints[0] = intvs[0];
	print_pcyc(lints);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
function convert_f(field,
	row, i)
{
	row[0] = split(field, row, /[ 	]*/);
	for (i = 1; i <= row[0]; ++i)
	{
		printf "%s", convert(row[i]);
		if (i+1 <= row[0])
			printf " ";
	}

	delete_array(row);
}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	for (i = 1; i <= NF; ++i)
	{
		if (is_a_field(i))
			pcyc($i);
		else
			printf("%s", $i);
		if (i+1 <= NF)
			printf ":";
	}

	printf("\n");
}
BEGIN {
	FS=":";
	convert_fields="1";
	cv_fields[0] = 0;
	contours = 0;
}
#
# $Log: CINT.awk,v $
# Revision 0.0  1996/04/20 16:00:27  nicb
# Initial Revision
#
#
