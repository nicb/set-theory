#
# $Id: m.awk 38 2014-02-12 14:44:53Z nicb $
#
# m.awk: multiply the incoming pattern (in numeric form) to the given
# multiplication factor - command line:
#
#	awk -f m.awk [convert_fields="n1,n2,..."] <file>
#
# where <file> is a collection of records in the following format:
#
# 1) records are a on one line, a collection of colon-separated fields
# 2) any number of fields of pitches in absolute numeric format can
#    be specified with the convert_fields string variable; these fields
#    must be space-separated sequences of pitches (default for convert_fields:
#    "1")
# 3) the field of multiplying values (space-separated pitches in absolute
#    form) can be set with the numeric variable m_field (default: 2)
#
function print_multiply(line,
	i)
{
	for (i = 1; i <= line[0]; ++i)
	{
		printf("%d", line[i]);
		if (i+1 <= line[0])
			printf " ";
	}
}
function multiply(line, tfact,
	intvs, i)
{
	intvs[0] = split (line, intvs, " ");

	for (i = 1; i <= intvs[0]; ++i)
		intvs[i] *= tfact;

	print_multiply(intvs);
	delete_array(intvs);
}
function delete_array(array,
	i)
{
	for (i = 1; i <= array[0]; ++i)
		delete array[i];

	delete array[0];
}
#function convert_f(field,
#	row, i)
#{
#	row[0] = split(field, row, /[ 	]*/);
#	for (i = 1; i <= row[0]; ++i)
#	{
#		printf "%s", convert(row[i]);
#		if (i+1 <= row[0])
#			printf " ";
#	}

#	delete_array(row);
#}
function is_a_field(num,
	i, result)
{
	result = 0;

	for (i = 1; i <= cv_fields[0]; ++i)
		if (num == cv_fields[i])
		{
			result = num;
			break;
		}

	return result;
}
NR == 1 {
	cv_fields[0] = split(convert_fields, cv_fields, ",");
}
{
	if ($m_field != "")
		m_fields_string = $m_field;

	m_fields[0] = split(m_fields_string, m_fields, " ");

	for (j = 1; j <= m_fields[0]; ++j)
	{
		for (i = 1; i <= NF; ++i)
		{
			if (is_a_field(i))
				multiply($i,m_fields[j]);
			else
				printf("%s", $i);
			if (i+1 <= NF)
				printf ":";
		}
		printf("\n");
	}

	delete_array(m_fields);
}
BEGIN {
	FS=":";
	convert_fields="1";
	m_field=2;
	cv_fields[0] = 0;
	m_fields[0] = 0;
	m_fields_string = "-1";
}
#
# $Log: m.awk,v $
# Revision 0.0  1996/04/20 16:00:27  nicb
# Initial Revision
#
#
