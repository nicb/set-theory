#
# $Id: primeform.awk 38 2014-02-12 14:44:53Z nicb $
#
# primeform.awk: this program reduces a set to its prime form
# by first putting it into normal order and then transposing it
# to its level 0
#
# Usage (for example):
#
# echo '7,11,9,3' | awk -f primeform.awk
#
function printn_row(row,
	i)
{
	printf "%d", row[1];
	for(i = 2; i <= row[0]; ++i)
		printf ",%d", row[i];
}
function print_row(row)
{
	printn_row(row);
	printf "\n";
}
function copy_row(src, dest,
	i)
{
	for (i = 0; i <= src[0]; ++i)
		dest[i] = src[i];
}
function ascending_sort(row,
	num_notes, i, j, temp)
{
	num_notes = row[0];

	for (i = 2; i <= num_notes; ++i)
		for (j = i; j > 1 && row[j-1] > row[j]; j--)
		{
			# swap row[j-1] and row[j]
			temp = row[j-1];
			row[j-1] = row[j];
			row[j] = temp;
		}
}
function circulate(row,
	i, num_notes, first)
{
	num_notes = row[0];
	first = row[1];
	for (i = 2; i <= num_notes; ++i)
		row[i-1] = row[i];
	row[num_notes] = first + 12;
}
function normal_order(row,
	best_order, i, best_diff, diff, num_notes, j, bdiff, cdiff)
{
	best_diff = 1000;
	num_notes = row[0];
	ascending_sort(row);
	for (i = 1; i <= num_notes; ++i)
	{
printf "perm n.%d: ", i; print_row(row);
		diff = row[num_notes] - row[1];
		bdiff = best_diff;
		cdiff = diff;
		for (j = 2; cdiff == bdiff && j <= num_notes; ++j)
		{
			cdiff = row[j] - row[1];
			bdiff = best_order[j] - best_order[1];
printf "aargh! same diff!: cdiff = %d, bdiff = %d\n", cdiff, bdiff;
			if (cdiff < bdiff)
			{
				diff = cdiff;
				break;
			}
		}
		if (diff < best_diff)
		{
printf "perm n.%d: ", i; printn_row(row); printf " better (%d) than (%d) ", diff, best_diff; print_row(best_order);
			copy_row(row, best_order);
			best_diff = best_order[num_notes] - best_order[1];
		}
		circulate(row);
	}
	copy_row(best_order, row);

printf "winner (best diff = %d) is: ", best_diff; print_row(row);

	return best_diff;
}
function level_0(row,
	num_notes, t0, i)
{
	num_notes = row[0];
	t0 = row[1];

	for (i = 1; i <= num_notes; ++i)
		row[i] = row[i] - t0;
}
function invert_row(row,
	i)
{
	for (i = 1; i <= row[0]; ++i)
		row[i] = (12 - row[i]) % 12;
}
{
	input_row[0] = split($1, input_row, ",");
	normal_order(input_row);
	sd = level_0(input_row);
printf "sd: %d\n", sd;
	copy_row(input_row, inverted_row);
	invert_row(inverted_row);
	normal_order(inverted_row);
	id = level_0(inverted_row);
printf "id: %d\n", id;
	if (sd < id)
		print_row(input_row);
	else if (sd == id)
	{
		for (x = 2; x <= input_row[0] &&
			((input_row[x] - input_row[x-1]) == (inverted_row[x] - inverted_row[x-1])); ++x)
			;
		if ((input_row[x]-input_row[x-1]) < (inverted_row[x]-inverted_row[x-1]))
			print_row(input_row);
		else
			print_row(inverted_row);
	}
	else
		print_row(inverted_row);
}
