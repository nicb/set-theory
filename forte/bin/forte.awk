#
# $Id: forte.awk 38 2014-02-12 14:44:53Z nicb $
#
# forte.awk: handles the forte sets and displays them in readable format
#
# Usage: awk -f forte.awk [3-9].dat
#
function m_12(i)
{
	if (i > 11)
		i -= 12;
	else if (i < 0)
		i += 12;

	return i;
}
function calc_index(n)
{
	if (n < 0)
		n += 12;
	while (n > 6)
		n = 12 - n;

	return n;
}
function calc_ivector(vector, vct, ivec_num)
{
	for (i = 1; i <= ivec_num; ++i)
	{
		for (j = 1; j < i; ++j)
		{
			num = calc_index(vector[i] - vector[j]);
			if (num > 0)
				vct[num] += 1;
		}
	}
}
function clear_vector(clrv)
{
	for (i = 0; i <= 128; ++i)
		delete clrv[i];
}
function accumulate_vectors(dest, src,
	i)
{
	for (i = 1; i <= 6; ++i)
		dest[i] += src[i];
}
function load_vector(vect, vload, start, end)
{
	for (i = 1; i <= end; ++i)
		vload[i] = vect[start+i];
}
/^-->/ {
	print;
	next;
}
BEGIN {
	FS = ":";
}
{
	nf = split($2, vect, ",");
	clear_vector(totals);
	clear_vector(v);
	calc_ivector(vect, v, nf);
	printf ("[%s](%s):[%1d%1d%1d%1d%1d%1d]\n", $1, $2, v[1],v[2],v[3],v[4],v[5],v[6]);
}
#
# $Log: forte.awk,v $
# Revision 0.0  1995/08/06  10:44:25  nicb
# Initial Revision
#
#
